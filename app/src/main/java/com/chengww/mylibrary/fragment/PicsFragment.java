package com.chengww.mylibrary.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chengww.mylibrary.R;
import com.chengww.mylibrary.base.LazyFragment;
import com.chengww.mylibrary.base.WrapContentLinearLayoutManager;
import com.chengww.mylibrary.ui.ResizableImageView;

import java.io.File;
import java.util.List;

/**
 * Created by chengww on 2017/7/16.
 */

public class PicsFragment extends LazyFragment {

    private RecyclerView mRecyclerView;
    private List<File> pics;

    public static PicsFragment newInstance(List<File> pics) {

        Bundle args = new Bundle();
        args.putBoolean(LazyFragment.INTENT_BOOLEAN_LAZYLOAD,true);
        PicsFragment fragment = new PicsFragment();
        fragment.setArguments(args);
        fragment.pics = pics;

        return fragment;
    }

    @Override
    protected void initView() {
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new WrapContentLinearLayoutManager(getActivity()));
    }

    @Override
    protected void doBusiness(Context context) {
        SimpleAdapter adapter = new SimpleAdapter(pics);
        adapter.bindToRecyclerView(mRecyclerView);
    }

    @Override
    public int bindLayout() {
        return R.layout.fragment_pics;
    }

    @Override
    public void widgetClick(View v) {

    }

    class SimpleAdapter extends BaseQuickAdapter<File, BaseViewHolder> {

        public SimpleAdapter(@Nullable List<File> data) {
            super(R.layout.recycler_item_pic_view, data);
        }

        @Override
        protected void convert(BaseViewHolder helper, File item) {
            ResizableImageView image = helper.getView(R.id.image);

            Glide.with(PicsFragment.this).load(item).skipMemoryCache(true).into(image);

        }


    }

    @Override
    public void onPause() {
        super.onPause();
        onDestroy();
    }

}
