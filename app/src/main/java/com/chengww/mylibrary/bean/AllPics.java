package com.chengww.mylibrary.bean;


import java.io.File;
import java.util.List;

/**
 * Created by chengww on 2017/7/16.
 */

public class AllPics {

    private static AllPics allPics;
    public static AllPics newInstance() {

        if (allPics == null)
            allPics = new AllPics();

        return allPics;
    }
    private List<List<File>> pics;

    public List<List<File>> getPics() {
        return pics;
    }

    public void setPics(List<List<File>> pics) {
        this.pics = pics;
    }

}
