package com.chengww.mylibrary.adapter;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chengww.mylibrary.R;

import java.util.List;

/**
 * Created by chengww on 2017/5/4.
 */

public class BannerAdapter extends PagerAdapter {
    public List<View> getViewArrayList() {
        return viewArrayList;
    }

    public void setViewArrayList(List<View> viewArrayList) {
        this.viewArrayList = viewArrayList;
    }

    public List<String> getmTitleList() {
        return mTitleList;
    }

    public void setmTitleList(List<String> mTitleList) {
        this.mTitleList = mTitleList;
    }

    private List<View> viewArrayList;
    private List<String> mTitleList;

    public BannerAdapter(List<View> viewArrayList) {
        this.viewArrayList = viewArrayList;
    }

    public BannerAdapter(List<View> viewArrayList, List<String> mTitleList) {
        this.viewArrayList = viewArrayList;
        this.mTitleList = mTitleList;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        RecyclerView mRecycler = (RecyclerView) viewArrayList.get(position);
        Log.e("qqq-mRecycler",mRecycler.toString());
        BaseQuickAdapter adapter = (BaseQuickAdapter) mRecycler.getAdapter();
        int count = adapter.getItemCount();
        Log.e("qqq-count",count+"");
        for (int i = 0; i < count; i++) {
            ImageView item = (ImageView) adapter.getViewByPosition(i,R.id.image);
            Log.e("qqq-ImageView",item.toString());
//            item.setImageBitmap(null);
            releaseImageViewResouce(item);
        }
        container.removeView(viewArrayList.get(position));
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = viewArrayList.get(position);
        container.addView(view);
        return view;
    }

    @Override
    public int getCount() {
        return viewArrayList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (mTitleList == null || mTitleList.size() <= 0)
            return null;
        return mTitleList.get(position);//页卡标题
    }

    /**
     * 释放图片资源的方法
     *
     * @param imageView
     */
    public void releaseImageViewResouce(ImageView imageView) {
        if (imageView == null) return;

        Drawable drawable = imageView.getDrawable();
        if (drawable != null && drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            Bitmap bitmap = bitmapDrawable.getBitmap();
            if (bitmap != null && !bitmap.isRecycled()) {
                bitmap.recycle();
                bitmap = null;
            }
        }
        imageView.destroyDrawingCache();
        System.gc();
    }

}
