package com.chengww.mylibrary;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chengww.mylibrary.adapter.BannerAdapter;
import com.chengww.mylibrary.base.BaseActivity;
import com.chengww.mylibrary.base.WrapContentLinearLayoutManager;
import com.chengww.mylibrary.bean.AllPics;
import com.chengww.mylibrary.ui.LazyViewPager;
import com.chengww.mylibrary.ui.ResizableImageView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by chengww on 2017/7/16.
 */

public class PicsViewActivity extends BaseActivity {

    private LazyViewPager mViewPager;

    private List<View> viewArrayList;

    private int index;

    public static final String INDEX = "INDEX";

    @Override
    protected void onLoaded(Context context) {

    }

    @Override
    protected void onLoading(Context context) {
        List<List<File>> pics = AllPics.newInstance().getPics();
        if (pics != null){
            for (int i = 0; i < pics.size(); i++) {
                RecyclerView mRecyclerView = new RecyclerView(this);
                mRecyclerView.setLayoutManager(new WrapContentLinearLayoutManager(this));
                SimpleAdapter adapter = new SimpleAdapter(pics.get(i));
//                adapter.bindToRecyclerView(mRecyclerView);
                mRecyclerView.setAdapter(adapter);
                viewArrayList.add(mRecyclerView);
            }
        }
        mViewPager.setAdapter(new BannerAdapter(viewArrayList));
        mViewPager.setCurrentItem(index,false);
    }

    @Override
    public void initParams(Bundle params) {
        index = params.getInt(INDEX);
    }

    @Override
    public int bindLayout() {
        return R.layout.activity_pics_view;
    }

    @Override
    public void initView(View view) {
        mViewPager = $(R.id.view_pager);
        viewArrayList = new ArrayList<>();
        hidetoolBar();
    }

    @Override
    public void widgetClick(View v) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setAllowFullScreen(true);
        super.onCreate(savedInstanceState);
    }

    class SimpleAdapter extends BaseQuickAdapter<File, BaseViewHolder> {

        public SimpleAdapter(@Nullable List<File> data) {
            super(R.layout.recycler_item_pic_view, data);
        }

        @Override
        protected void convert(BaseViewHolder helper, File item) {
            ResizableImageView image = helper.getView(R.id.image);

            Glide.with(PicsViewActivity.this).load(item).into(image);

        }


    }
}
